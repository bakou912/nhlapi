# NHL API Documentation
---
In light of the recent addition of records.nhl.com and the effort to document 
the API that it relies upon it became necessary to split up the files for sanity
sake.

This effort is purely to make it easier for stats nerds and the like to make
use of the wonderful trove of information the NHL provides to us but in a much
more digestible form. I am only one person so if you see something I have missed
please feel free to open up a PR and I will get it merged.
